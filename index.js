console.log('Hello World');



	
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

        const addition = function(num1, num2){
            console.log(`The sum of ${num1} and ${num2} is:`, num1 + num2);
        }

        addition(5,15);


         const subtraction = function(num1, num2){
            console.log(`The difference of ${num1} and ${num2} is:`, num1 - num2);
        }

        subtraction(20, 5);


	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.

	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.

	//  	Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.

    const multiplication = function(num1, num2){
        console.log(`The product of ${num1} and ${num2} is:`, num1 * num2);
        return num1 * num2;
    }

    multiplication(50, 10);

    const division = function(num1, num2){
        console.log(`The quotient of ${num1} and ${num2} is:`, num1 / num2);
        return num1 / num2;
    }

    division(50, 10);


	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.


    const getCircleArea = function(radius){
        const area = Math.PI * (radius**2);
        console.log(`The result of getting the area of circle with ${radius} is: `, area.toFixed(2));

        return area.toFixed(2);
    }


    getCircleArea(15);

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.

	// Log the value of the circleArea variable in the console.

	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.

	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.
	

    const getAverage = function(numbers){
       
        const average = (numbers.reduce((a,b)=> a+b))/ numbers.length;
        console.log( `The average of ${numbers[0]}, ${numbers[1]}, ${numbers[2]} and ${numbers[3]} is: `, average);

        return average;
    }

    const numbers = [20, 40, 60, 80];
    getAverage(numbers);




	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.

	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.


    const getScore = function(score, totalScore){
        const percentage = (score/totalScore) * 100;

        const isPass = percentage > 75;

        console.log(`Is ${score}/${totalScore} a passing score?`);
        console.log(isPass);

        return isPass;
    }

    getScore(40,50);